# cloud_lab_2

## Description
This is repository for cloud lab 2. More details can see [Azure-lab2](Azure-lab2.pdf).

- (i) locally deployed numericalintegral [vm-machine-folder](myproject) and its result stores in [vm-machine-result-folder](microservice)
- (ii) VM scaleset with 2 VMs where you shutdown the VM running the workload after 1 minute [vm-scaleset-result-folder](vm_scaleset)
- (iii) autoscale webapp initially configured with 1 instance and max 3 [web-scaleset-result-folder](webapp.scaleset)
- (iv) autoscale function. Save locust output. (Code: [function-scaleset-folder](function_scale_flask), example code:  [function-example-folder](function_scale)) The - result are stored at [function-scaleset-result-folder](function)
- (v) Mapreduce code are stored in [mapReduce-folder](mapReduce)

## Visuals

Here shows the result for the first four tasks.
![Results for the first four tasks](image.png)

## Authors and acknowledgment

Author: Jiehong MO

Acknowledgment: Learn_Azure_in_a_Month_of_Lunches.pdf

## Project status

Finished