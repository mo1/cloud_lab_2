import azure.functions as func
import azure.durable_functions as df
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

myApp = df.DFApp(http_auth_level=func.AuthLevel.ANONYMOUS)

@myApp.activity_trigger(input_name="input")
def GetInputDataFn(input: str):
    # get the content from blob
    # account_url = "https://mapreducestorageaccount.blob.core.windows.net"
    # default_credential = DefaultAzureCredential()
    # Create the BlobServiceClient object
    # blob_service_client = BlobServiceClient(account_url, credential=default_credential)
    # List the blobs in the container
    sas_url = "https://mapreducestorageaccount.blob.core.windows.net/mapreducecontainer?sp=rli&st=2023-12-20T22:58:00Z&se=2024-02-01T06:58:00Z&spr=https&sv=2022-11-02&sr=c&sig=hrUm310aDSgsRVTW9QJAvFv5yFV2wnmhAfzFne2cObU%3D"
    mapReduceContainer_client = ContainerClient.from_container_url(container_url=sas_url)
    # mapReduceContainer_client = blob_service_client.get_container_client("mapreducepubliccontainer")
    blob_list = mapReduceContainer_client.list_blobs()
    print(blob_list)
    line_list = []
    i = 0
    for blob in blob_list:
        # encoding param is necessary for readall() to return str, otherwise it returns bytes
        downloader = mapReduceContainer_client.download_blob(blob=blob.name, max_concurrency=1, encoding='UTF-8')
        blob_text = downloader.readall()
        block_text_list = blob_text.split('\n')
        for line in block_text_list:
            line_list.append((i, line))
            i = i + 1
    return line_list

        

# An HTTP-Triggered Function with a Durable Functions Client binding
@myApp.route(route="orchestrators/{functionName}")
@myApp.durable_client_input(client_name="client")
async def http_start(req: func.HttpRequest, client):
    function_name = req.route_params.get('functionName')
    instance_id = await client.start_new(function_name)
    response = client.create_check_status_response(req, instance_id)
    return response

# Orchestrator
@myApp.orchestration_trigger(context_name="context")
def map_reduce_orchestrator(context):
    # how to parallel
    line_list = yield context.call_activity("GetInputDataFn", "")
    mappingTasks = []
    for line_tuple in line_list:
        mappingTasks.append(context.call_activity("mapper", line_tuple))
    mappingResults = yield context.task_all(mappingTasks)

    shuffleResults = yield context.call_activity("shuffle", mappingResults)

    tasks = []
    for shuffleResult in shuffleResults.items():
        tasks.append(context.call_activity("reducer", shuffleResult))

    results = yield context.task_all(tasks)

    return results


# Activity
@myApp.activity_trigger(input_name="city")
def hello(city: str):
    return "Hello " + city

@myApp.activity_trigger(input_name="line")
def mapper(line: tuple):
    words = line[1].split()
    word_list  = list()
    for w in words:
        word_list.append((w,1))
    return word_list
@myApp.activity_trigger(input_name="mapperoutputs")    
def shuffle(mapperoutputs: list):
    word_dic = {}
    for mapperoutput in mapperoutputs:
        for word in mapperoutput:
            if 	word[0] in word_dic.keys():
                word_list = word_dic[word[0]]
                word_list.append(word[1])
                word_dic[word[0]] = word_list
            else:
                word_list = [word[1]]
                word_dic[word[0]] = word_list
    return word_dic

@myApp.activity_trigger(input_name="shuffleoutput")    
def reducer(shuffleoutput: tuple):
    return (shuffleoutput[0], sum(shuffleoutput[1]))



