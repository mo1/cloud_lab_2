# Deploy a webapp

# Change the pricing plan to Premium P1V2

# Automatic Scale
Choose Custom autoscale
Configure your webapp to do auto-scaling: Rule-based auto-scaling.

Register Subscription microsoft.insights
az provider register --namespace microsoft.insights

{
    "location": "East US",
    "tags": {},
    "properties": {
        "name": "MMMMMMorty_asp_2562-Autoscale-661",
        "enabled": true,
        "targetResourceUri": "/subscriptions/a9580f93-d6e4-4b2d-ae58-356a85dcd282/resourceGroups/webapp.scaleset/providers/Microsoft.Web/serverfarms/MMMMMMorty_asp_2562",
        "profiles": [
            {
                "name": "Auto created default scale condition",
                "capacity": {
                    "minimum": "1",
                    "maximum": "3",
                    "default": "1"
                },
                "rules": [
                    {
                        "scaleAction": {
                            "direction": "Increase",
                            "type": "ChangeCount",
                            "value": "1",
                            "cooldown": "PT5M"
                        },
                        "metricTrigger": {
                            "metricName": "CpuPercentage",
                            "metricNamespace": "microsoft.web/serverfarms",
                            "metricResourceUri": "/subscriptions/a9580f93-d6e4-4b2d-ae58-356a85dcd282/resourceGroups/webapp.scaleset/providers/Microsoft.Web/serverFarms/MMMMMMorty_asp_2562",
                            "operator": "GreaterThan",
                            "statistic": "Average",
                            "threshold": 50,
                            "timeAggregation": "Average",
                            "timeGrain": "PT1M",
                            "timeWindow": "PT10M",
                            "Dimensions": [],
                            "dividePerInstance": false
                        }
                    }
                ]
            }
        ],
        "notifications": [],
        "targetResourceLocation": "East US"
    },
    "id": "/subscriptions/a9580f93-d6e4-4b2d-ae58-356a85dcd282/resourceGroups/webapp.scaleset/providers/microsoft.insights/autoscalesettings/MMMMMMorty_asp_2562-Autoscale-661",
    "name": "MMMMMMorty_asp_2562-Autoscale-661",
    "type": "Microsoft.Insights/autoscaleSettings"
}

Click Save

I cannot connect with app insight