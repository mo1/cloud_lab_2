import azure.functions as func
import logging
import math

app = func.FunctionApp(http_auth_level=func.AuthLevel.FUNCTION)

@app.route(route="numericalintegralservice")
def flask_function_scale(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    a = req.params.get('a')
    if not a:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            a = req_body.get('a')
    
    b = req.params.get('b')
    if not b:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            b = req_body.get('b')

    if a and b:
        a = float(a)
        b = float(b)
        if b < a:
            a, b = b,a
        #make sure b>=a
        array = [10, 100, 100, 1000, 10000, 100000, 1000000]
        # caculate 
        sum = [0, 0, 0, 0, 0, 0, 0]
        for i in range(7):
            for j in range(array[i]):
                y = abs(math.sin((b-a)/array[i]*j+a))
                x = (b-a)/array[i]
                sum[i] = sum[i] + x*y
    
        return f'<h1>Results<h1><body><p>a = {a}</p><p> b= {b}</p> <p>N = [10, 100, 100, 1000, 10000, 100000, 1000000]</p> <p>Result: {sum}\n</p></body>'
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass a and b in the query string or in the request body for a personalized response.",
             status_code=200
        )