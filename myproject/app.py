from flask import Flask
import math

app = Flask(__name__)

@app.route("/")
def hello():
    return "<h1>Hello, welcome to the sin caculation!</h1>"

@app.route("/numericalintegralservice/<a>/<b>")
def cal_sin(a, b):
    a = float(a)
    b = float(b)
    if b < a:
        a, b = b,a
    #make sure b>=a
    array = [10, 100, 100, 1000, 10000, 100000, 1000000]
    # caculate 
    sum = [0, 0, 0, 0, 0, 0, 0]
    for i in range(7):
        for j in range(array[i]):
            y = abs(math.sin((b-a)/array[i]*j+a))
            x = (b-a)/array[i]
            sum[i] = sum[i] + x*y
    
    return f'<h1>Results<h1><body><p>a = {a}</p><p> b= {b}</p> <p>N = [10, 100, 100, 1000, 10000, 100000, 1000000]</p> <p>Result: {sum}\n</p></body>'
