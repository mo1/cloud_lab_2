from locust import HttpUser, task

class HelloWorldUser(HttpUser):
    @task
    def hello_world(self):
        self.client.get("/api/numericalintegralservice?code=dH6zO2GjTBwnJdDIW9zvmmgOG4wvdu9yD0kc3mENtds0AzFu1vtL7w%3D%3D&a=0&&b=3.14159")