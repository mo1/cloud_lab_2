import requests
# https://functionscaleset.azurewebsites.net/api/numericalintegralservice?code=dH6zO2GjTBwnJdDIW9zvmmgOG4wvdu9yD0kc3mENtds0AzFu1vtL7w%3D%3D&a=0&&b=3.14159
url = "https://functionscaleset.azurewebsites.net/api/numericalintegralservice?code=dH6zO2GjTBwnJdDIW9zvmmgOG4wvdu9yD0kc3mENtds0AzFu1vtL7w%3D%3D"
params = {"a": "0", "b": "3.14159"}

response = requests.get(url, params=params)

if response.status_code == 200:
    # The request was successful
    print("Response:", response.text)
else:
    # There was an error with the request
    print("Error:", response.status_code)
