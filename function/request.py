import requests

url = "http://localhost:7071/api/test_http_cloud_function"
params = {"name": "morty"}

response = requests.get(url, params=params)

if response.status_code == 200:
    # The request was successful
    print("Response:", response.text)
else:
    # There was an error with the request
    print("Error:", response.status_code)
